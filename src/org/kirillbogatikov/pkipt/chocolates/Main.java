package org.kirillbogatikov.pkipt.chocolates;

import java.util.Scanner;

/**
 * @author Кирилл Испольнов, 16ит18К
 */
public class Main {
    private static final String PROGRAM_RESPONSE_FORMAT = "Вы сможете купить аж %d шоколад%s";
    private static final String INFINITY_DIATHESIS_ERROR = "Адрес магазинчика не подскажите? Вы сможете обменивать шоколадки вечно...";
    private static final String INPUT_WRAPS_MESSAGE = "Сколько обёрток можно обменять на одну шоколадку? ";
    private static final String SAVE_YOUR_PESO_ERROR = "Кажется, Ваше финансовое состояние не позволит купить даже одну шоколадку. Экономьте свои песо";
    private static final String INPUT_PRICE_MESSAGE = "Введите стоимость одной шоколадки: ";
    private static final String INPUT_MONEY_MESSAGE = "Введите сумму денег, которая имеется у Вас на руках: ";
    
    static Scanner scanner = new Scanner(System.in);
    
    public static void main(String[] args) {
        System.out.print(INPUT_MONEY_MESSAGE);
        int money = scanner.nextInt();
        System.out.print(INPUT_PRICE_MESSAGE);
        int price = scanner.nextInt();
        if(money < price) {
            System.out.println(SAVE_YOUR_PESO_ERROR);
            return;
        }
        
        System.out.print(INPUT_WRAPS_MESSAGE);
        int wraps = scanner.nextInt();
        
        if(wraps <= 1) {
            System.out.println(INFINITY_DIATHESIS_ERROR);
            return;
        }
        
        int chocolates = money / price;
        int wraps_count = chocolates;
        
        while(wraps_count >= wraps) {
            wraps_count -= wraps - 1;
            chocolates++;
        }
        
        System.out.printf(PROGRAM_RESPONSE_FORMAT, chocolates, wordCase(chocolates));
    }
    
    public static String wordCase(int chocolates) {
        int units = chocolates % 10;
        
        if(chocolates == 11 || units > 4) {
            return "ок";
        }
        
        if(units == 1) {
            return "ку";
        }
        
        return "ки";
    }
}
